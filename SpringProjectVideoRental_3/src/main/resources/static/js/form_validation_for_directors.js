function validateForm_2() {
    'use strict'
    // Получите все формы, к которым мы хотим применить пользовательские стили проверки Bootstrap
    const forms = document.querySelectorAll('.needs-validation');
    const position = document.getElementById("position");
    // Зацикливайтесь на них и предотвращайте отправку
    Array.prototype.slice.call(forms)
        .forEach(function (form) {
            form.addEventListener('submit', function (event) {
                if (!form.checkValidity()) {
                    event.preventDefault()
                    event.stopPropagation()
                }
                if (position.value === 'default') {
                    alert("Пожалуйста, выберете должность!");
                    event.preventDefault()
                    event.stopPropagation()
                    return false;
                }
                form.classList.add('was-validated')
            }, false)
        })
}