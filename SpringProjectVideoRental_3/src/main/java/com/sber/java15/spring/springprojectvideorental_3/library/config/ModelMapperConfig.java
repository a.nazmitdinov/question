package com.sber.java15.spring.springprojectvideorental_3.library.config;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import static org.modelmapper.config.Configuration.AccessLevel.PRIVATE;

@Configuration
public class ModelMapperConfig {
    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        /** Проставляем свои собственные значения. Иначе будут использованы дефолтные параметры */
        modelMapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.STRICT)    // стратегия соответствия (д.б. строгой)
                .setFieldMatchingEnabled(true) // сопоставление полей по названию
                .setSkipNullEnabled(true) // пропускаем пустые значения полей
                .setFieldAccessLevel(PRIVATE); // будем использовать приватный уровень доступа к полям

        return modelMapper;
    }
}
