package com.sber.java15.spring.springprojectvideorental_3.library.MVC.controller;

import com.sber.java15.spring.springprojectvideorental_3.library.dto.DirectorDTO;
import com.sber.java15.spring.springprojectvideorental_3.library.dto.FilmDTO;
import com.sber.java15.spring.springprojectvideorental_3.library.dto.FilmSearchDTO;
import com.sber.java15.spring.springprojectvideorental_3.library.dto.FilmWithDirectorDTO;
import com.sber.java15.spring.springprojectvideorental_3.library.exception.MyDeleteException;
import com.sber.java15.spring.springprojectvideorental_3.library.service.FilmService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static com.sber.java15.spring.springprojectvideorental_3.library.constants.UserRolesConstants.ADMIN;

@Slf4j
@Controller
@RequestMapping("/films")
public class MVCFilmController {
    private final FilmService filmService;

    public MVCFilmController(FilmService filmService) { this.filmService = filmService; }

    @GetMapping("")
    public String getAll(@RequestParam(value = "page", defaultValue = "1") int page,
                         @RequestParam(value = "size", defaultValue = "5") int pageSize,
                         Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "filmTitle"));
        Page<FilmWithDirectorDTO> films;
        final String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        if (ADMIN.equalsIgnoreCase(userName)) {
            films = filmService.getAllFilmsWithDirectors(pageRequest);
        }
        else {
            films = filmService.getAllNotDeletedFilmsWithDirectors(pageRequest);
        }
        model.addAttribute("films", films);
        return "films/viewAllFilms";
    }

    @GetMapping("/{id}")
    public String getOne(@PathVariable Long id,
                         Model model) {
        log.info("Title: " + filmService.getFilmWithDirectors(id).getFilmTitle());
        model.addAttribute("film", filmService.getFilmWithDirectors(id));
        return "films/viewFilm";
    }

    @GetMapping("/add")
    public String create() {

        return "films/addFilm";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("filmForm") FilmDTO newFilm,
                         @RequestParam (required = false, value = "file") MultipartFile file) {
        if (file != null && file.getSize() > 0) {
            filmService.create(newFilm, file);
        }
        else {
            filmService.create(newFilm);
        }
        return "redirect:/films";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id,
                         Model model) {
        model.addAttribute("film", filmService.getOne(id));
        return "films/updateFilm";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("filmForm") FilmDTO filmDTO,
                         @RequestParam MultipartFile file) {
//        log.info(filmDTO.toString());
        if (file != null && file.getSize() > 0) {
            filmService.update(filmDTO, file);
        }
        else {
            filmService.update(filmDTO);
        }
        return "redirect:/films";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) throws MyDeleteException {
        filmService.deleteSoft(id);
        return "redirect:/films";
    }

    @GetMapping("/restore/{id}")
    public String restore(@PathVariable Long id) {
        filmService.restore(id);
        return "redirect:/films";
    }

    @PostMapping("/search")
    public String searchFilms(@RequestParam(value = "page", defaultValue = "1") int page,
                              @RequestParam(value = "size", defaultValue = "5") int pageSize,
                              @ModelAttribute("filmSearchForm") FilmSearchDTO filmSearchDTO,
                              Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "film_title"));
        model.addAttribute("films",filmService.findFilms(filmSearchDTO, pageRequest));
        return "films/viewAllFilms";
    }

    /**
     * Метод для поиска книги по ФИО автора (редирект по кнопке "Посмотреть книги" на странице автора)
     *
     * @param page      - текущая страница
     * @param pageSize  - количество объектов на странице
     * @param directorDTO - ДТО режиссера
     * @param model     - модель
     * @return - форму со списком всех фильмов подходящих под критерии (по фио режиссера)
     */
    @PostMapping("/search/director")
    public String searchFilms(@RequestParam(value = "page", defaultValue = "1") int page,
                              @RequestParam(value = "size", defaultValue = "5") int pageSize,
                              @ModelAttribute("directorSearchForm") DirectorDTO directorDTO,
                              Model model) {
        FilmSearchDTO filmSearchDTO = new FilmSearchDTO();
        filmSearchDTO.setDirectorsFio(directorDTO.getDirectorsFio());
        return searchFilms(page, pageSize, filmSearchDTO, model);
    }

    @GetMapping(value = "/download", produces = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody
    public ResponseEntity<Resource> downloadFilm(@Param(value = "filmId") Long filmId) throws IOException {
        FilmDTO filmDTO = filmService.getOne(filmId);
        Path path = Paths.get(filmDTO.getOnlineCopyPath());
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));

        return ResponseEntity.ok()
                .headers(headers(path.getFileName().toString()))
                .contentLength(path.toFile().length())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
//              .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }

    private HttpHeaders headers(final String name) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + name);
        headers.add("Cache-Control", "no-cache, no-store");
        headers.add("Expires", "0");
        return headers;
    }
}
