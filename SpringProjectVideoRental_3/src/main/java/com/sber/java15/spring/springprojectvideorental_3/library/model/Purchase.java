package com.sber.java15.spring.springprojectvideorental_3.library.model;

public enum Purchase {
    Yes("Оплачена"),
    No("Не оплачена");

    private final String purchaseTextDisplay;

    Purchase(String text) {

        this.purchaseTextDisplay = text;
    }

    public String getPurchaseTextDisplay() {

        return purchaseTextDisplay;
    }
}
