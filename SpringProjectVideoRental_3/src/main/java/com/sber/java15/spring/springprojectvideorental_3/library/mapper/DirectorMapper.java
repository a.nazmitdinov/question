package com.sber.java15.spring.springprojectvideorental_3.library.mapper;

import com.sber.java15.spring.springprojectvideorental_3.library.dto.DirectorDTO;
import com.sber.java15.spring.springprojectvideorental_3.library.model.Directors;
import com.sber.java15.spring.springprojectvideorental_3.library.model.GenericModel;
import com.sber.java15.spring.springprojectvideorental_3.library.repository.FilmRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class DirectorMapper extends GenericMapper<Directors, DirectorDTO> {

    private final FilmRepository filmRepository;

    protected DirectorMapper(ModelMapper modelMapper,
                             FilmRepository filmRepository) {
        super(Directors.class, DirectorDTO.class, modelMapper);
        this.filmRepository = filmRepository;
    }

    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Directors.class, DirectorDTO.class)
                .addMappings(m -> m.skip(DirectorDTO::setFilmIDs))
                .setPostConverter(toDTOConverter());                // toDTOConverter() - метод в GenericMapper

        modelMapper.createTypeMap(DirectorDTO.class, Directors.class)
                .addMappings(m -> m.skip(Directors::setFilms))
                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(DirectorDTO source, Directors destination) {
        if(!Objects.isNull(source.getFilmIDs())) {
            destination.setFilms(filmRepository.findAllById(source.getFilmIDs()));
        }
        else {
            destination.setFilms(Collections.emptyList());
        }
    }

    @Override
    protected void mapSpecificFields(Directors source, DirectorDTO destination) {

        destination.setFilmIDs(getIds(source));
    }

    @Override
    protected List<Long> getIds(Directors directors) {
        return Objects.isNull(directors) || Objects.isNull(directors.getFilms())
                ? Collections.emptyList()
                : directors.getFilms().stream()
                .map(GenericModel::getId)// получаем Id книг
                .collect(Collectors.toList());
    }
}
