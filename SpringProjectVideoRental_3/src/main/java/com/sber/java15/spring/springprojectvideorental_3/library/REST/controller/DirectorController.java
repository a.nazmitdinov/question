package com.sber.java15.spring.springprojectvideorental_3.library.REST.controller;

import com.sber.java15.spring.springprojectvideorental_3.library.dto.AddFilmDTO;
import com.sber.java15.spring.springprojectvideorental_3.library.dto.DirectorDTO;
import com.sber.java15.spring.springprojectvideorental_3.library.model.Directors;
import com.sber.java15.spring.springprojectvideorental_3.library.service.DirectorService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rest/directors")
@Tag(name = "Режиссеры",
        description = "Контроллер для работы с режиссерами фильмов из видеотеки")
@SecurityRequirement(name = "Bearer Authentication")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class DirectorController extends GenericController <Directors, DirectorDTO> {

    public DirectorController(DirectorService directorService) {

        super(directorService);
    }

    @Operation(description = "Добавить фильм режиссеру")
    @RequestMapping(value = "/addFilm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectorDTO> addFilm (@RequestParam (value = "filmId") Long filmId,
                                              @RequestParam(value = "directorId") Long directorId) {
        AddFilmDTO addFilmDTO = new AddFilmDTO();
        addFilmDTO.setDirectorId(directorId);
        addFilmDTO.setFilmId(filmId);
        return ResponseEntity.status(HttpStatus.OK).body(((DirectorService) service).addFilm(addFilmDTO));
    }
}
