package com.sber.java15.spring.springprojectvideorental_3.library.mapper;

import com.sber.java15.spring.springprojectvideorental_3.library.dto.GenericDTO;
import com.sber.java15.spring.springprojectvideorental_3.library.model.GenericModel;
import jakarta.annotation.PostConstruct;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

@Component
public abstract class GenericMapper <E extends GenericModel, D extends GenericDTO>
        implements Mapper<E, D> {

    private final Class <E> entityClass;
    private final Class <D> dtoClass;
    protected final ModelMapper modelMapper;

    protected GenericMapper(Class<E> entityClass,
                            Class<D> dtoClass,
                            ModelMapper modelMapper) {
        this.entityClass = entityClass;
        this.dtoClass = dtoClass;
        this.modelMapper = modelMapper;
    }

    // *********************************************************************************************
    @Override
    public E toEntity(D dto) {
        return Objects.isNull(dto) // если DTO пустой, верни null. Иначе замапь данные из DTO в Entity class
                ? null
                : modelMapper.map(dto, entityClass);
    }

    @Override
    public D toDTO(E entity) {
        return Objects.isNull(entity) // если Entity пустой, верни null. Иначе замапь данные из Entity в DTO class
                ? null
                : modelMapper.map(entity, dtoClass);
    }

    @Override
    public List<E> toEntities(List<D> dtos) {

        return dtos.stream().map(this::toEntity).toList(); // преобразовываем к списку
    }

    @Override
    public List<D> toDTOs(List<E> entities) {

        return entities.stream().map(this::toDTO).toList(); // преобразовываем к списку
    }

    // *********************************************************************************************

    /*
    Конвертер начинает работать когда основной алгоритм маппинга применен
    */
    protected Converter<D, E> toEntityConverter() {
        return context -> {
            D source = context.getSource();
            E destination = context.getDestination();
            mapSpecificFields(source, destination);

            return context.getDestination();
        };
    }
    protected Converter<E, D> toDTOConverter() {
        return context -> {
            E source = context.getSource();
            D destination = context.getDestination();
            mapSpecificFields(source, destination);

            return context.getDestination();
        };
    }

    // *********************************************************************************************

    /** Описываем в методе как себя вести конкретному мапперу (реализация для конкретных сущностей)
     Author -> AuthorDTO, Book -> BookDTO и т.д.

     !!!  2 перегруженных метода  !!!
     */
    protected abstract void mapSpecificFields(E source, D destination);

    /** Описываем в методе как себя вести конкретному мапперу (реализация для конкретных сущностей)
     Author -> AuthorDTO, Book -> BookDTO и т.д. */
    protected abstract void mapSpecificFields(D source, E destination);

    /**
     * Настройка маппера (что делать и что вызывать в случае несовпадения типов данных source/destination)
     */
    @PostConstruct
    protected abstract void setupMapper();

    protected abstract List<Long> getIds(E entity);
}
