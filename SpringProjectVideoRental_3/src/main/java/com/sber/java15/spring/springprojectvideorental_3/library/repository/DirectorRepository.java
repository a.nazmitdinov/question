package com.sber.java15.spring.springprojectvideorental_3.library.repository;

import com.sber.java15.spring.springprojectvideorental_3.library.model.Directors;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface DirectorRepository
        extends GenericRepository <Directors> {

    Page<Directors> findAllByDirectorsFioContainsIgnoreCaseAndIsDeletedFalse(String directorsFio,
                                                                       Pageable pageable);
}
