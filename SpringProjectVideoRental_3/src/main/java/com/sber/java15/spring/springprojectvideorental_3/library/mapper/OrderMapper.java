package com.sber.java15.spring.springprojectvideorental_3.library.mapper;

import com.sber.java15.spring.springprojectvideorental_3.library.dto.OrderDTO;
import com.sber.java15.spring.springprojectvideorental_3.library.model.Orders;
import com.sber.java15.spring.springprojectvideorental_3.library.repository.FilmRepository;
import com.sber.java15.spring.springprojectvideorental_3.library.repository.UserRepository;
import com.sber.java15.spring.springprojectvideorental_3.library.service.FilmService;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;

import java.util.List;

@Component
public class OrderMapper
        extends GenericMapper<Orders, OrderDTO> {
    private final FilmRepository filmRepository;
    private final UserRepository userRepository;
    private final FilmService filmService;

    protected OrderMapper(ModelMapper mapper,
                          FilmRepository filmRepository,
                          UserRepository userRepository,
                          FilmService filmService) {
        super(Orders.class, OrderDTO.class, mapper);
        this.filmRepository = filmRepository;
        this.userRepository = userRepository;
        this.filmService = filmService;
    }

    @PostConstruct
    public void setupMapper() {
        super.modelMapper.createTypeMap(Orders.class, OrderDTO.class)
                .addMappings(m -> m.skip(OrderDTO::setUserId))
                .addMappings(m -> m.skip(OrderDTO::setFilmId))
                .addMappings(m -> m.skip(OrderDTO::setFilmDTO))
                .setPostConverter(toDTOConverter());

        super.modelMapper.createTypeMap(OrderDTO.class, Orders.class)
                .addMappings(m -> m.skip(Orders::setUsers))
                .addMappings(m -> m.skip(Orders::setFilms))
                .setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(OrderDTO source, Orders destination) {
        destination.setFilms(filmRepository.findById(source.getFilmId()).orElseThrow(() -> new NotFoundException("Фильм не найден")));
        destination.setUsers(userRepository.findById(source.getUserId()).orElseThrow(() -> new NotFoundException("Пользователь не найден")));
    }

    @Override
    protected void mapSpecificFields(Orders source, OrderDTO destination) {
        destination.setUserId(source.getUsers().getId());
        destination.setFilmId(source.getFilms().getId());
        destination.setFilmDTO(filmService.getOne(source.getFilms().getId()));
    }

    @Override
    protected List<Long> getIds(Orders entity) {
        throw new UnsupportedOperationException("Метод недоступен");
    }
}
