package com.sber.java15.spring.springprojectvideorental_3.library.repository;

import com.sber.java15.spring.springprojectvideorental_3.library.model.Users;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface UserRepository extends GenericRepository <Users>{

//    select * from users where login = ?
//    @Query(nativeQuery = true,
//           value = "select * from users where login = :login") // выполнится SQL-запрос
//    @Query(nativeQuery = false,
//         value = "select login from User where login = :login") // работаем с моделью User (выполнится запрос hibernate query language)
    Users findUserByLogin(String login);
    Users findUserByEmail(String email);
    Users findUserByChangePasswordToken(String uuid);

    @Query(nativeQuery = true,
            value = """
                 select u.*
                 from users u
                 where u.first_name ilike '%' || coalesce(:firstName, '%') || '%'
                 and u.last_name ilike '%' || coalesce(:lastName, '%') || '%'
                 and u.login ilike '%' || coalesce(:login, '%') || '%'
                  """)
    Page<Users> searchUsers(String firstName,
                           String lastName,
                           String login,
                           Pageable pageable);

    @Query(nativeQuery = true,
            value = """
                 select distinct email
                 from users u join orders o on u.id = o.user_id
                 where cast (o.rent_date as date) + o.rent_period < now()
                 and o.returned = false
                 and u.is_deleted = false
                 """)
    List<String> getDelayedEmails();
}
