package com.sber.java15.spring.springprojectvideorental_3.library.service;

import com.sber.java15.spring.springprojectvideorental_3.library.dto.AddFilmDTO;
import com.sber.java15.spring.springprojectvideorental_3.library.dto.DirectorDTO;
import com.sber.java15.spring.springprojectvideorental_3.library.mapper.DirectorMapper;
import com.sber.java15.spring.springprojectvideorental_3.library.model.Directors;
import com.sber.java15.spring.springprojectvideorental_3.library.repository.DirectorRepository;
import com.sber.java15.spring.springprojectvideorental_3.library.repository.FilmRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DirectorService extends GenericService<Directors, DirectorDTO> {

    private final DirectorRepository directorRepository;

    public DirectorService(DirectorRepository directorRepository,
                           DirectorMapper directorMapper) {
        super(directorRepository, directorMapper);
        this.directorRepository= directorRepository;
    }

    public DirectorDTO addFilm(final AddFilmDTO addFilmDTO) {
        DirectorDTO director = getOne(addFilmDTO.getDirectorId());
        director.getFilmIDs().add(addFilmDTO.getFilmId());
        update(director);
        return director;
    }

    public Page<DirectorDTO> searchDirectors(final String fio,
                                         Pageable pageable) {
        Page<Directors> directors = directorRepository.findAllByDirectorsFioContainsIgnoreCaseAndIsDeletedFalse(fio, pageable);
        List<DirectorDTO> result = mapper.toDTOs(directors.getContent());
        return new PageImpl<>(result, pageable, directors.getTotalElements());
    }
}
