package com.sber.java15.spring.springprojectvideorental_3.library.config.jwt;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.function.Function;
@Component
@Slf4j
public class JWTTokenUtil {

    /** 7 * 24 * 60 * 60 * 1000 = 604800000 (1 неделя в миллисекундах (время жизни токена)) */
    public static final long JWT_TOKEN_VALIDITY = 604800000;

    /** Секрет для формирования подписи токена */
    private final String secret = "efghjmnaponvfmkjjqQmyRtcx";

    private static final ObjectMapper objectMapper = getDefaultObjectMapper();

    private static ObjectMapper getDefaultObjectMapper() {

        return new ObjectMapper();
    }

    public String generateToken(final UserDetails payload) {
        return Jwts.builder()
                .setSubject(payload.toString())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY))
                .signWith(SignatureAlgorithm.HS512, secret) // Алгоритм шифрования HS512 + секрет для формирования подписи
                .compact();
    }

    /** Проверка на то, что токен истек, или нет */
    private Boolean isTokenExpired(final String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    /** Нужно достать expirationDate из токена */
    private Date getExpirationDateFromToken(final String token) {
        return getClaimsFromToken(token, Claims::getExpiration);
    }

    /** Подтверждение токена */
    public Boolean validateToken(final String token,
                                 UserDetails userDetails) {
        final String userName = getUsernameFromToken(token);
        return (userName.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }

    /** Достаем username из токена (из claims (Claims == Payload (просто по другому называется)) */
    public String getUsernameFromToken(final String token) {
        String claim = getClaimsFromToken(token, Claims::getSubject);
        JsonNode claimJSON = null;
        try {
            claimJSON = objectMapper.readTree(claim);
        }
        catch (JsonProcessingException e) {
            log.error("JWTTokenUtil#getUserNameFromToken(): {}", e.getMessage());
        }
        if (claimJSON != null) {
            return claimJSON.get("username").asText();
        }
        else {
            return null;
        }
    }

    /** Достаем role из токена (из Claims) */
    public String getRoleFromToken(final String token) {
        String claim = getClaimsFromToken(token, Claims::getSubject);
        JsonNode claimJSON = null;
        try {
            claimJSON = objectMapper.readTree(claim);
        }
        catch (JsonProcessingException e) {
            log.error("JWTTokenUtil#getUserNameFromToken(): {}", e.getMessage());
        }
        if (claimJSON != null) {
            return claimJSON.get("user_role").asText();
        }
        else {
            return null;
        }
    }

    private <T> T getClaimsFromToken(final String token, Function<Claims, T> claimResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimResolver.apply(claims);
    }

    /** Для получения любой информации из токена, нужно предъявить секретный ключ */
    private Claims getAllClaimsFromToken(final String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }
}
