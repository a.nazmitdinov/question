package com.sber.java15.spring.springprojectvideorental_3.library.REST.controller;

import com.sber.java15.spring.springprojectvideorental_3.library.dto.AddFilmDTO;
import com.sber.java15.spring.springprojectvideorental_3.library.dto.FilmDTO;
import com.sber.java15.spring.springprojectvideorental_3.library.model.Films;
import com.sber.java15.spring.springprojectvideorental_3.library.service.FilmService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/films")
@Tag(name = "Фильмы",
        description = "Контроллер для работы с названиями фильмов из видеотеки")
//@SecurityRequirement(name = "Bearer Authentication")
//@CrossOrigin(origins = "*", allowedHeaders = "*")
public class FilmController
        extends GenericController <Films, FilmDTO> {

    public FilmController(FilmService filmService) {

        super(filmService);
    }

    @Operation(description = "Добавить режиссера к фильму ")
    @RequestMapping(value = "/addDirector", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FilmDTO> addDirector (@RequestParam(value = "filmId") Long filmId,
                                                @RequestParam(value = "directorId") Long directorId) {
        AddFilmDTO filmDTO = new AddFilmDTO();
        filmDTO.setDirectorId(directorId);
        filmDTO.setFilmId(filmId);
        return ResponseEntity.status(HttpStatus.OK).body(((FilmService) service).addDirector(filmDTO));
    }
}