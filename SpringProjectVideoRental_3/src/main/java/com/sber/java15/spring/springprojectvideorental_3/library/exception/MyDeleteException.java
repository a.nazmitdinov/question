package com.sber.java15.spring.springprojectvideorental_3.library.exception;

public class MyDeleteException  extends Exception {
    public MyDeleteException(final String message) {

        super(message);
    }
}
