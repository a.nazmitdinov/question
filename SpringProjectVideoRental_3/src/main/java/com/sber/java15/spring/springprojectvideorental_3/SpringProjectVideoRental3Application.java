package com.sber.java15.spring.springprojectvideorental_3;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
//@EnableScheduling
public class SpringProjectVideoRental3Application implements CommandLineRunner {

	public static void main(String[] args) {

		SpringApplication.run(SpringProjectVideoRental3Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		System.out.println("Swagger runs at http://localhost:9090/swagger-ui/index.html");
		System.out.println("WEB-page runs at http://localhost:9090");
	}
}

/** С помощью Thymeleaf (шаблонизатора) происходит интеграция java-кода с помощью
 * специальных макросов в HTML-код страницы.
 */