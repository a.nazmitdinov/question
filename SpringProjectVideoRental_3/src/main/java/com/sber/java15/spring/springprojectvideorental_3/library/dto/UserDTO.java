package com.sber.java15.spring.springprojectvideorental_3.library.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class UserDTO extends GenericDTO{
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private String middleName;
    private LocalDate birthDate;
    private String phone;
    private String address;
    private String email;
    private LocalDateTime createdWhen;
    private String changePasswordToken;
    private RoleDTO role;
    private List<Long> userFilmRent;
    private boolean isDeleted;
}
