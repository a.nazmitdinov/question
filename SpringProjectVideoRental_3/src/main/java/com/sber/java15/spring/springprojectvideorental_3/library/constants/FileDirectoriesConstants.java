package com.sber.java15.spring.springprojectvideorental_3.library.constants;

public interface FileDirectoriesConstants {
    String FILMS_UPLOAD_DIRECTORY = "SpringProjectVideoRental_3/files/films";
}
