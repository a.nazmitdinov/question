package com.sber.java15.spring.springprojectvideorental_3.library.repository;

import com.sber.java15.spring.springprojectvideorental_3.library.model.Orders;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository
        extends GenericRepository<Orders> {
    Page<Orders> getOrdersByUsersId(Long id, Pageable pageRequest); }
