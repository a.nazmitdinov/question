package com.sber.java15.spring.springprojectvideorental_3.library.dto;

import com.sber.java15.spring.springprojectvideorental_3.library.model.Genre;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class FilmSearchDTO {
    private String filmTitle;
    private String directorsFio;
    private Genre genre;
}
