package com.sber.java15.spring.springprojectvideorental_3.library.service;

import com.sber.java15.spring.springprojectvideorental_3.library.dto.FilmDTO;
import com.sber.java15.spring.springprojectvideorental_3.library.dto.OrderDTO;
import com.sber.java15.spring.springprojectvideorental_3.library.mapper.OrderMapper;
import com.sber.java15.spring.springprojectvideorental_3.library.model.Orders;
import com.sber.java15.spring.springprojectvideorental_3.library.model.Purchase;
import com.sber.java15.spring.springprojectvideorental_3.library.repository.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public class OrderService extends GenericService<Orders, OrderDTO> {
    private final FilmService filmService;

    protected OrderService(OrderRepository orderRepository,
                           OrderMapper orderMapper,
                           FilmService filmService) {
        super(orderRepository, orderMapper);
        this.filmService = filmService;
    }

    public OrderDTO rentFilm (final OrderDTO orderDTO) {
        FilmDTO filmDTO = filmService.getOne(orderDTO.getFilmId());
        filmService.update(filmDTO);
        long rentPeriod = orderDTO.getRentPeriod() != null ? orderDTO.getRentPeriod() : 14L;
        orderDTO.setRentDate(LocalDateTime.now());
        orderDTO.setReturned(false);
        orderDTO.setRentPeriod((int) rentPeriod);
        orderDTO.setReturnDate(LocalDateTime.now().plusDays(rentPeriod));
        orderDTO.setCreatedWhen(LocalDateTime.now());
        orderDTO.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        return mapper.toDTO(repository.save(mapper.toEntity(orderDTO)));
    }
    public Page<OrderDTO> listUserRentFilms(final Long id,
                                            final Pageable pageRequest) {
        Page<Orders> objects = ((OrderRepository) repository).getOrdersByUsersId(id, pageRequest);
        List<OrderDTO> results = mapper.toDTOs(objects.getContent());
//        log.info("1:" + results.toString());
//        log.info("2:" + pageRequest.toString());
//        log.info("3:" + mapper.toDTOs(objects.getContent()).toString());
        return new PageImpl<>(results, pageRequest, objects.getTotalElements());
    }

    public void returnFilm(final Long id) {
        OrderDTO orderDTO = getOne(id);
        orderDTO.setReturned(true);
        orderDTO.setReturnDate(LocalDateTime.now());
        FilmDTO filmDTO = orderDTO.getFilmDTO();
//        filmDTO.setAmount(filmDTO.getAmount() + 1); // НЕТ КОЛИЧЕСТВА
        update(orderDTO);
        filmService.update(filmDTO);
    }
}
