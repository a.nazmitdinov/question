package com.sber.java15.spring.springprojectvideorental_3.library.model;

public enum Genre {
    ACTION_MOVIE("Боевик"),
    WESTERN("Вестерн"),
    DETECTIVE("Детектив"),
    DRAMA("Драма"),
    HISTORICAL("Исторический"),
    COMEDY("Комедия"),
    MELODRAMA("Мелодрама"),
    SCIENCE_FICTION("Научная фантастика"),
    THRILLER("Триллер"),
    FANTASY("Фантастика"),
    HORROR("Фильм ужасов");

    private final String genreTextDisplay;
    Genre (String text) {

        this.genreTextDisplay = text;
    }
    public String getGenreTextDisplay() {

        return genreTextDisplay;
    }
}
