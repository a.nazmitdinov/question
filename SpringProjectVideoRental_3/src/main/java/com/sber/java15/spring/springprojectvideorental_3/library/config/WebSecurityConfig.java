package com.sber.java15.spring.springprojectvideorental_3.library.config;

import com.sber.java15.spring.springprojectvideorental_3.library.service.userdetails.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import java.util.Arrays;

import static com.sber.java15.spring.springprojectvideorental_3.library.constants.SecurityConstants.*;
import static com.sber.java15.spring.springprojectvideorental_3.library.constants.UserRolesConstants.*;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig {
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final CustomUserDetailsService customUserDetailsService;

    public WebSecurityConfig(BCryptPasswordEncoder bCryptPasswordEncoder,
                             CustomUserDetailsService customUserDetailsService) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.customUserDetailsService = customUserDetailsService;
    }

    @Bean
    public HttpFirewall httpFirewall() {
        StrictHttpFirewall firewall = new StrictHttpFirewall(); // строгий firewall
        firewall.setAllowedHttpMethods(Arrays.asList("GET", "POST", "PUT", "DELETE")); // с какими методами будет работать система
        firewall.setAllowBackSlash(true);            // разрешается в запросе указывать "\"
        firewall.setAllowUrlEncodedPercent(true);    // разрешается в запросе указывать "%"
        firewall.setAllowUrlEncodedSlash(true);      // разрешается в запросе указывать "/"
        firewall.setAllowSemicolon(true);            // разрешается в запросе указывать ";"
        return firewall;
    }

    /** Фильтрация http-запросов */
    /**
     * Cross-Origin Resource Sharing (CORS или “совместное использование ресурсов различными источниками”) —
     * это контролируемый и применяемый в принудительном порядке клиентом (браузером) механизм обеспечения
     * безопасности на основе HTTP.
     * Cross-site request forgery (CSRF) — это веб-атаки, которые используют лазейки в SOP политике, и CORS
     * не блокирует их. Заключается атака в том, что злоумышленник запускает вредоносные скрипты в браузере
     * жертвы и таким образом, жертва совершает непреднамеренные действия от своего лица.
     */
    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
            .cors().disable()                        // Отключим cors-фильтр. Он нам пока не нужен.
            .csrf().disable()                        // Отключим csrf-фильтр. Он нам пока не нужен.
                //Настройка http-запросов - кому/куда можно/нельзя
                .authorizeHttpRequests((requests) -> requests
                        .requestMatchers(RESOURCES_WHITE_LIST.toArray(String[]::new)).permitAll()
                        .requestMatchers(FILMS_WHITE_LIST.toArray(String[]::new)).permitAll()
                        .requestMatchers(DIRECTORS_WHITE_LIST.toArray(String[]::new)).permitAll()
                        .requestMatchers(USERS_WHITE_LIST.toArray(String[]::new)).permitAll()
                        .requestMatchers(FILMS_PERMISSION_LIST.toArray(String[]::new)).hasAnyRole(ADMIN, VIDEO_RENTAL_EMPLOYEE)
                        .requestMatchers(DIRECTORS_PERMISSION_LIST.toArray(String[]::new)).hasAnyRole(ADMIN, VIDEO_RENTAL_EMPLOYEE)
                        .requestMatchers(USERS_PERMISSION_LIST.toArray(String[]::new)).hasAnyRole(VIDEO_RENTAL_EMPLOYEE, USER)
                        .anyRequest().authenticated()
                )
                //Настройка для входа в систему
                .formLogin((form) -> form
                        .loginPage("/login")
                //Перенаправление на главную страницу после успешной авторизации
                        .defaultSuccessUrl("/")                       // После успешной регистрации перенаправляем на главную страницу
                        .permitAll()                                  // Страница логина д.б. доступна всегда и всем
                )
                //Настройка для выхода из системы
                .logout((logout) -> logout
                        .logoutUrl("/logout")
                        .logoutSuccessUrl("/login")                   // Что показать когда пользователь выйдет из системы
                        .invalidateHttpSession(true)                  // Настройка http-сессии
                        .deleteCookies("JSESSIONID") // Какие cookie мы хотим чистить
                        .permitAll()                                  // Страница логина д.б. доступна всегда и всем
                        .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                );

        return http.build();
    }
    @Autowired
    protected void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(customUserDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }
}
