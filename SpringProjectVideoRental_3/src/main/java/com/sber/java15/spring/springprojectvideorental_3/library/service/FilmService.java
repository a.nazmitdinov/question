package com.sber.java15.spring.springprojectvideorental_3.library.service;

import com.sber.java15.spring.springprojectvideorental_3.library.constants.Errors;
import com.sber.java15.spring.springprojectvideorental_3.library.dto.AddFilmDTO;
import com.sber.java15.spring.springprojectvideorental_3.library.dto.FilmDTO;
import com.sber.java15.spring.springprojectvideorental_3.library.dto.FilmSearchDTO;
import com.sber.java15.spring.springprojectvideorental_3.library.dto.FilmWithDirectorDTO;
import com.sber.java15.spring.springprojectvideorental_3.library.exception.MyDeleteException;
import com.sber.java15.spring.springprojectvideorental_3.library.mapper.FilmMapper;
import com.sber.java15.spring.springprojectvideorental_3.library.mapper.FilmWithDirectorsMapper;
import com.sber.java15.spring.springprojectvideorental_3.library.model.Directors;
import com.sber.java15.spring.springprojectvideorental_3.library.model.Films;
import com.sber.java15.spring.springprojectvideorental_3.library.repository.FilmRepository;
import com.sber.java15.spring.springprojectvideorental_3.library.utils.FileHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.webjars.NotFoundException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class FilmService extends GenericService<Films, FilmDTO> {

    private final FilmRepository repository;
    private final FilmWithDirectorsMapper filmWithDirectorsMapper;

    public FilmService(FilmRepository repository,
                       FilmMapper mapper,
                       FilmWithDirectorsMapper filmWithDirectorsMapper) {
        super(repository, mapper);
        this.repository = repository;
        this.filmWithDirectorsMapper = filmWithDirectorsMapper;
    }

    public Page<FilmWithDirectorDTO> getAllFilmsWithDirectors(Pageable pageable) {
        Page<Films> filmsPaginated = repository.findAll(pageable);
        List<FilmWithDirectorDTO> result = filmWithDirectorsMapper.toDTOs(filmsPaginated.getContent());
        return new PageImpl<>(result, pageable, filmsPaginated.getTotalElements());
    }

    public Page<FilmWithDirectorDTO> getAllNotDeletedFilmsWithDirectors(Pageable pageable) {
        Page<Films> filmsPaginated = repository.findAllByIsDeletedFalse(pageable);
        List<FilmWithDirectorDTO> result = filmWithDirectorsMapper.toDTOs(filmsPaginated.getContent());
        return new PageImpl<>(result, pageable, filmsPaginated.getTotalElements());
    }

    public FilmDTO addDirector(final AddFilmDTO addFilmDTO) {
        FilmDTO film = getOne(addFilmDTO.getFilmId());
        film.getDirectorIDs().add(addFilmDTO.getDirectorId());
        update(film);
        return film;
    }

    public Page<FilmWithDirectorDTO> findFilms(FilmSearchDTO filmSearchDTO,
                                               Pageable pageRequest) {
        String genre = filmSearchDTO.getGenre() != null ? String.valueOf(filmSearchDTO.getGenre().ordinal()) : null;
        Page<Films> filmsPaginated = repository.searchFilms(genre,
                                                            filmSearchDTO.getFilmTitle(),
                                                            filmSearchDTO.getDirectorsFio(),
                                                            pageRequest);
        List<FilmWithDirectorDTO> result = filmWithDirectorsMapper.toDTOs(filmsPaginated.getContent());
        return new PageImpl<>(result, pageRequest, filmsPaginated.getTotalElements());
    }

    /**
     * @param id
     * @return
     */
    public FilmWithDirectorDTO getFilmWithDirectors(final Long id) {
//        log.info("1: " + super.getOne(id).toString());
//        log.info("2: " + mapper.toEntity(super.getOne(id)).toString());
//        log.info("3: " + filmWithDirectorsMapper.toDTO(mapper.toEntity(super.getOne(id))).toString());
        return filmWithDirectorsMapper.toDTO(mapper.toEntity(super.getOne(id)));
    }

    /** Сохраняем данные о фильме с загрузкой файла */
    public FilmDTO create(final FilmDTO newFilm,
                          MultipartFile file) {
        String fileName = FileHelper.createFile(file);
        newFilm.setOnlineCopyPath(fileName);
        newFilm.setCreatedWhen(LocalDateTime.now());
        newFilm.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());

        return mapper.toDTO(repository.save(mapper.toEntity(newFilm)));
    }

    /** Сохраняем данные о фильме без загрузки файла */
    public FilmDTO create(final FilmDTO newFilm) {
        newFilm.setCreatedWhen(LocalDateTime.now());
        newFilm.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());

        return mapper.toDTO(repository.save(mapper.toEntity(newFilm)));
    }

    /** Обновляем данные о фильме с возможностью подгрузить файл */
    public FilmDTO update(final FilmDTO updatedFilm,
                          MultipartFile file) {
        String fileName = FileHelper.createFile(file);
        updatedFilm.setOnlineCopyPath(fileName);
        Films films = repository.findById(updatedFilm.getId()).get();
        List<Long> ids = new ArrayList<>();
        for (Directors directors : films.getDirectors()){
            ids.add(directors.getId());
        }
        updatedFilm.setDirectorIDs(ids);
        updatedFilm.setCreatedWhen(LocalDateTime.now());
        updatedFilm.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());

        return mapper.toDTO(repository.save(mapper.toEntity(updatedFilm)));
    }

    @Override
    public void deleteSoft(final Long id) throws MyDeleteException {
        Films film = repository.findById(id).orElseThrow(() -> new NotFoundException("Книги не найдено"));
        boolean filmCanBeDeleted = repository.checkFilmForDeletion(id);
//        boolean b1 = repository.findFilmByIdAndOrdersReturnedFalseAndIsDeletedFalse(id) == null;
        if (filmCanBeDeleted) {
            markAsDeleted(film);
            repository.save(film);
        }
        else {
            throw new MyDeleteException(Errors.Films.FILM_DELETE_ERROR);
        }
    }
}
