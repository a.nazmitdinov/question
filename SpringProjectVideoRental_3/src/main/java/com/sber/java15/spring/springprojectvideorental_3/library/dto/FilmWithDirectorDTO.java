package com.sber.java15.spring.springprojectvideorental_3.library.dto;

import lombok.*;

import java.util.Set;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class FilmWithDirectorDTO
        extends FilmDTO{
    private Set<DirectorDTO> directors;
}
