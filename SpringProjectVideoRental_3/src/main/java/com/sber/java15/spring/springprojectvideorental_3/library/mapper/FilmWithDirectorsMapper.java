package com.sber.java15.spring.springprojectvideorental_3.library.mapper;

import com.sber.java15.spring.springprojectvideorental_3.library.dto.FilmWithDirectorDTO;
import com.sber.java15.spring.springprojectvideorental_3.library.model.Films;
import com.sber.java15.spring.springprojectvideorental_3.library.repository.DirectorRepository;
import com.sber.java15.spring.springprojectvideorental_3.library.model.GenericModel;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class FilmWithDirectorsMapper
        extends GenericMapper<Films, FilmWithDirectorDTO> {

    private final DirectorRepository directorRepository;

    protected FilmWithDirectorsMapper(ModelMapper modelMapper,
                                      DirectorRepository directorRepository) {
        super(Films.class, FilmWithDirectorDTO.class, modelMapper);
        this.directorRepository = directorRepository;
    }

    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Films.class, FilmWithDirectorDTO.class)
                .addMappings(m -> m.skip(FilmWithDirectorDTO::setDirectorIDs)).setPostConverter(toDTOConverter());

        modelMapper.createTypeMap(FilmWithDirectorDTO.class, Films.class)
                .addMappings(m -> m.skip(Films::setDirectors)).setPostConverter(toEntityConverter());
    }
    @Override
    protected void mapSpecificFields(FilmWithDirectorDTO source, Films destination) {
        destination.setDirectors(directorRepository.findAllById(source.getDirectorIDs()));
    }

    @Override
    protected void mapSpecificFields(Films source, FilmWithDirectorDTO destination) {
        destination.setDirectorIDs(getIds(source));
    }

    @Override
    protected List<Long> getIds(Films entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getId())
                ? Collections.emptyList()
                : entity.getDirectors().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());
    }
}
