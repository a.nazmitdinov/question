package com.sber.java15.spring.springprojectvideorental_3.library.constants;

public interface UserRolesConstants {
    String ADMIN = "ADMIN";
    String USER = "USER";
    String VIDEO_RENTAL_EMPLOYEE = "VIDEO_RENTAL_EMPLOYEE";
}
