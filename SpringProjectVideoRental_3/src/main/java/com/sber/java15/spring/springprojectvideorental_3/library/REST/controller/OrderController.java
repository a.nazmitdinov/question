package com.sber.java15.spring.springprojectvideorental_3.library.REST.controller;

import com.sber.java15.spring.springprojectvideorental_3.library.dto.OrderDTO;
import com.sber.java15.spring.springprojectvideorental_3.library.model.Orders;
import com.sber.java15.spring.springprojectvideorental_3.library.service.OrderService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order/info")
@Tag(name = "Заказ фильмов",
        description = "Контроллер для работы с арендой/выдачей фильмов пользователям видеотеки")
public class OrderController extends GenericController <Orders, OrderDTO> {

    public OrderController (OrderService orderService) {
        super(orderService);
    }
}
