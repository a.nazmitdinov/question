package com.sber.java15.spring.springprojectvideorental_3.library.config;

import com.sber.java15.spring.springprojectvideorental_3.library.service.UserService;
import com.sber.java15.spring.springprojectvideorental_3.library.utils.MailUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class MailScheduler {
    private final UserService userService;
    private final JavaMailSender javaMailSender;
    
    public MailScheduler(UserService userService,
                         JavaMailSender javaMailSender) {
        this.userService = userService;
        this.javaMailSender = javaMailSender;
    }


    /** Сайт для проверки правильного указания времени в аннотации @Scheduled */
//         TODO: https://crontab.cronhub.io/

//    @Scheduled(cron = "0 0/1 * 1/1 * *") //каждую минуту
//    @Scheduled(cron = "0 0 10 * * *") //каждый день в 10 утра
    public void sendMailsToDebtors() {
        List<String> emails = userService.getUserEmailsWithDelayedRentDate();
        if (emails.size() > 0) {
            SimpleMailMessage simpleMailMessage = MailUtils.crateMailMessage(emails.toArray(new String[0]),
                                                                            "Напоминание о просрочке аренды фильма(ов)",
                                                                            "Вы - злостный нарушитель!!! Верните фильм!!!!");
            javaMailSender.send(simpleMailMessage);
        }
        log.info("Планировщик работает");
    }
}
