package com.sber.java15.spring.springprojectvideorental_3.library.mapper;

import com.sber.java15.spring.springprojectvideorental_3.library.dto.FilmDTO;
import com.sber.java15.spring.springprojectvideorental_3.library.model.Films;
import com.sber.java15.spring.springprojectvideorental_3.library.model.GenericModel;
import com.sber.java15.spring.springprojectvideorental_3.library.repository.DirectorRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class FilmMapper extends GenericMapper<Films, FilmDTO> {

    private final DirectorRepository directorRepository;

    protected FilmMapper(ModelMapper modelMapper,
                         DirectorRepository directorRepository) {
        super(Films.class, FilmDTO.class,modelMapper);
        this.directorRepository = directorRepository;
    }

    @PostConstruct
    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Films.class, FilmDTO.class)
                .addMappings(m -> m.skip(FilmDTO::setDirectorIDs)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(FilmDTO.class, Films.class)
                .addMappings(m -> m.skip(Films::setDirectors)).setPostConverter(toEntityConverter());

    }

    @Override
    protected void mapSpecificFields(FilmDTO source, Films destination) {
        if (!Objects.isNull(source.getDirectorIDs())) {
            destination.setDirectors(directorRepository.findAllById(source.getDirectorIDs()));
        }
        else {
            destination.setDirectors(Collections.emptyList());
        }
    }

    @Override
    protected void mapSpecificFields(Films source, FilmDTO destination) {

        destination.setDirectorIDs(getIds(source));
    }

    @Override
    protected List<Long> getIds(Films films) {
        return Objects.isNull(films) || Objects.isNull(films.getDirectors())
                ? null
                : films.getDirectors().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());
    }
}
