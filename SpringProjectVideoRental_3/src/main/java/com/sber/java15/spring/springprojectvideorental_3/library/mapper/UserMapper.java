package com.sber.java15.spring.springprojectvideorental_3.library.mapper;

import com.sber.java15.spring.springprojectvideorental_3.library.dto.UserDTO;
import com.sber.java15.spring.springprojectvideorental_3.library.model.GenericModel;
import com.sber.java15.spring.springprojectvideorental_3.library.model.Users;
import com.sber.java15.spring.springprojectvideorental_3.library.repository.OrderRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class UserMapper extends GenericMapper<Users, UserDTO> {

    private final OrderRepository orderRepository;

    protected UserMapper(ModelMapper modelMapper,
                         OrderRepository orderRepository) {
        super(Users.class, UserDTO.class, modelMapper);
        this.orderRepository = orderRepository;
    }

    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Users.class, UserDTO.class)
                .addMappings(m -> m.skip(UserDTO::setUserFilmRent)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(UserDTO.class, Users.class)
                .addMappings(m -> m.skip(Users::setOrders)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(UserDTO source, Users destination) {
        if (!Objects.isNull(source.getUserFilmRent())) {
            destination.setOrders(orderRepository.findAllById(source.getUserFilmRent()));
        }
        else {
            destination.setOrders(Collections.emptyList());
        }
    }

    @Override
    protected void mapSpecificFields(Users source, UserDTO destination) {

        destination.setUserFilmRent(getIds(source));
    }

    @Override
    protected List<Long> getIds(Users users) {
        return Objects.isNull(users) || Objects.isNull(users.getOrders())
                ? null
                : users.getOrders().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());
    }
}
