package com.sber.java15.spring.springprojectvideorental_3.library.config.jwt;

import com.sber.java15.spring.springprojectvideorental_3.library.service.userdetails.CustomUserDetailsService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static com.sber.java15.spring.springprojectvideorental_3.library.constants.SecurityConstants.*;
import static com.sber.java15.spring.springprojectvideorental_3.library.constants.UserRolesConstants.*;

//@Configuration
//@EnableWebSecurity
//@EnableMethodSecurity
//public class JWTSecurityConfig {
//    private final JWTTokenFilter jwtTokenFilter;
//    private final CustomUserDetailsService customUserDetailsService;
//
//    public JWTSecurityConfig(JWTTokenFilter jwtTokenFilter,
//                             CustomUserDetailsService customUserDetailsService) {
//        this.jwtTokenFilter = jwtTokenFilter;
//        this.customUserDetailsService = customUserDetailsService;
//    }
//
//    @Bean
//    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
//        return http.cors(AbstractHttpConfigurer::disable)
//                   .csrf(AbstractHttpConfigurer::disable)
//                /** Настройка http-запросов - кому/куда можно/нельзя */
//                .authorizeHttpRequests((requests) -> requests
//                        .requestMatchers(RESOURCES_WHITE_LIST.toArray(String[]::new)).permitAll()
//                        .requestMatchers(USERS_REST_WHITE_LIST.toArray(String[]::new)).permitAll()
////                        .requestMatchers(FILMS_WHITE_LIST.toArray(String[]::new)).permitAll()
////                        .requestMatchers(DIRECTORS_WHITE_LIST.toArray(String[]::new)).permitAll()
////                        .requestMatchers(USERS_WHITE_LIST.toArray(String[]::new)).permitAll()
////                        .requestMatchers(FILMS_PERMISSION_LIST.toArray(String[]::new)).hasAnyRole(ADMIN, VIDEO_RENTAL_EMPLOYEE)
////                        .requestMatchers(DIRECTORS_PERMISSION_LIST.toArray(String[]::new)).hasAnyRole(ADMIN, VIDEO_RENTAL_EMPLOYEE)
//                        .requestMatchers("/rest/directors/**").hasAnyRole(ADMIN, USER)
//                        .anyRequest().authenticated()
//                )
//                .exceptionHandling()
////                .authenticationEntryPoint()
//                .and()
//                .sessionManagement(
//                        session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
//                )
//                /** JWT Token Filter VALID OR NOT */
//                .addFilterBefore(jwtTokenFilter, UsernamePasswordAuthenticationFilter.class)
//                .userDetailsService(customUserDetailsService)
//                .build();
//    }
//
//    @Bean
//    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
//        return authenticationConfiguration.getAuthenticationManager();
//    }
//}

