package com.sber.java15.spring.springprojectvideorental_3.library.repository;

import com.sber.java15.spring.springprojectvideorental_3.library.model.Films;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface FilmRepository
        extends GenericRepository <Films> {
    @Query(nativeQuery = true,
            value = """
                 select distinct f.*
                 from films f
                 left join film_directors fd on f.id = fd.film_id
                 left join directors d on d.id = fd.director_id
                 where f.film_title ilike '%' || coalesce(:film_title, '%')  || '%'
                    and cast(f.genre as char) like coalesce(:genre, '%')
                    and coalesce(d.directors_fio, '%') ilike '%' || coalesce(:directors_fio, '%')  || '%'
                    and f.is_deleted = false
                 """)
    Page<Films> searchFilms(@Param(value = "genre") String genre,
                            @Param(value = "film_title") String filmTitle,
                            @Param(value = "directors_fio") String directorsFio,
                            Pageable pageRequest);

    Films findFilmByIdAndOrdersReturnedFalseAndIsDeletedFalse(final Long id);

    @Query("""
          select case when count(f) > 0 then false else true end
          from Films f join Orders o on f.id = o.films.id
          where f.id = :id and o.returned = false
          """)
    boolean checkFilmForDeletion(final Long id);
}