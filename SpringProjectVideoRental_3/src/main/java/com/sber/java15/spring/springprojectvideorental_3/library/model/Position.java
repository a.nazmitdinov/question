package com.sber.java15.spring.springprojectvideorental_3.library.model;

public enum Position {
    DIRECTOR("Режиссер"),
    ACTOR("Актер"),
    SCREENWRITER("Сценарист"),
    PRODUCER("Продюсер");

    private final String positionTextDisplay;

    Position(String text) {
        this.positionTextDisplay = text;
    }

    public String getPositionTextDisplay() {
        return positionTextDisplay;
    }
}


