package com.sber.java15.spring.springprojectvideorental_3.library.MVC.controller;

import com.sber.java15.spring.springprojectvideorental_3.library.dto.OrderDTO;
import com.sber.java15.spring.springprojectvideorental_3.library.service.FilmService;
import com.sber.java15.spring.springprojectvideorental_3.library.service.OrderService;
import com.sber.java15.spring.springprojectvideorental_3.library.service.userdetails.CustomUserDetails;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@Slf4j
@Hidden
@RequestMapping("/rent")
public class MVCOrderController {
    private final OrderService orderService;
    private final FilmService filmService;

    public MVCOrderController(OrderService orderService,
                              FilmService filmService) {
        this.orderService = orderService;
        this.filmService = filmService;
    }

    @GetMapping("/film/{filmId}")
    public String rentFilm(@PathVariable Long filmId,
                           Model model) {
        model.addAttribute("film", filmService.getOne(filmId));
        return "userFilms/rentFilm";
    }

    @PostMapping("/film")
    public String rentFilm(@ModelAttribute("rentFilmInfo") OrderDTO orderDTO) {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext()
                                                                                       .getAuthentication()
                                                                                       .getPrincipal();
        orderDTO.setUserId(Long.valueOf(customUserDetails.getUserId()));
        orderService.rentFilm(orderDTO);
        return "redirect:/rent/user-films/" + customUserDetails.getUserId();
    }

    @GetMapping("/return-film/{id}")
    public String returnFilm(@PathVariable Long id) {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (customUserDetails.getUserId() != null) { // книгу возвращает user
        orderService.returnFilm(id);
        return "redirect:/rent/user-films/" + customUserDetails.getUserId();}
        else {
            OrderDTO orderDTO = orderService.getOne(id); // книгу за user возвращает admin
            orderService.returnFilm(id);
            return "redirect:/rent/user-films/" + orderDTO.getUserId();
        }
    }

    @GetMapping("/user-films/{id}")
    public String userFilms(@RequestParam(value = "page", defaultValue = "1") int page,
                            @RequestParam(value = "size", defaultValue = "8") int pageSize,
                            @PathVariable Long id,
                            Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize);
        Page<OrderDTO> rentInfoDTOPage = orderService.listUserRentFilms(id, pageRequest);
        model.addAttribute("rentFilms", rentInfoDTOPage);
        model.addAttribute("userId", id);
//        log.info("3: " + orderService.listUserRentFilms(id, pageRequest).toString());
//        log.info("4: " + pageRequest.toString());
//        log.info("5: " + rentInfoDTOPage.toString());
        return "userFilms/viewAllUserFilms";
    }
}