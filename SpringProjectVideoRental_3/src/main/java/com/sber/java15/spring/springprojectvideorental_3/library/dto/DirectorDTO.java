package com.sber.java15.spring.springprojectvideorental_3.library.dto;

import com.sber.java15.spring.springprojectvideorental_3.library.model.Directors;
import com.sber.java15.spring.springprojectvideorental_3.library.model.Films;
import com.sber.java15.spring.springprojectvideorental_3.library.model.Position;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DirectorDTO extends GenericDTO{

    private String directorsFio;
    private Position position;
    private List<Long> filmIDs;

    public DirectorDTO(Directors directors) {
        this.id = directors.getId();
        this.createdBy = directors.getCreatedBy();
        this.createdWhen = directors.getCreatedWhen();
        this.directorsFio = directors.getDirectorsFio();
        this.position = Position.valueOf(directors.getPosition());
        List<Films> films = directors.getFilms();
        List<Long> filmIds = new ArrayList<>();
        films.forEach(f -> filmIds.add(f.getId()));
        this.filmIDs = filmIDs;
    }
}
