package com.sber.java15.spring.springprojectvideorental_3.library.MVC.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MVCMainController {
    @GetMapping("/")
    public String index() {

        return "index";
    }
}