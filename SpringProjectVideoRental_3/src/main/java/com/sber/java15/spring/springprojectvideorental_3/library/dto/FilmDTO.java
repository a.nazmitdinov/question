package com.sber.java15.spring.springprojectvideorental_3.library.dto;

import com.sber.java15.spring.springprojectvideorental_3.library.model.Genre;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class FilmDTO extends GenericDTO{

    private String filmTitle;
    private LocalDate premierYear;
    private String country;
    private String onlineCopyPath;
    private Genre genre;
    private List<Long> directorIDs;
//    private boolean isDeleted;
}
