package com.sber.java15.spring.springprojectvideorental_3.library.service;

import com.sber.java15.spring.springprojectvideorental_3.library.dto.DirectorDTO;
import com.sber.java15.spring.springprojectvideorental_3.library.model.Directors;
import com.sber.java15.spring.springprojectvideorental_3.library.model.Position;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface DirectorTestData {
    DirectorDTO DIRECTOR_DTO_1 = new DirectorDTO("directorsFio1",
                                                 Position.valueOf("position1"),
                                                 new ArrayList<>());

    DirectorDTO DIRECTOR_DTO_2 = new DirectorDTO("directorsFio2",
                                                Position.valueOf("position2"),
                                                new ArrayList<>());

    DirectorDTO DIRECTOR_DTO_3_DELETED = new DirectorDTO("directorsFio3",
                                                         Position.valueOf("position3"),
                                                         new ArrayList<>());
    
    List<DirectorDTO> DIRECTOR_DTO_LIST = Arrays.asList(DIRECTOR_DTO_1, DIRECTOR_DTO_2, DIRECTOR_DTO_3_DELETED);


    Directors DIRECTOR_1 = new Directors("director1",
                                 "position1",
                                 null);

    Directors DIRECTOR_2 = new Directors("director2",
                                 "position2",
                                 null);

    Directors DIRECTOR_3 = new Directors("director3",
                                 "position3",
                                 null);
    
    List<Directors> DIRECTORS_LIST = Arrays.asList(DIRECTOR_1, DIRECTOR_2, DIRECTOR_3);
}
