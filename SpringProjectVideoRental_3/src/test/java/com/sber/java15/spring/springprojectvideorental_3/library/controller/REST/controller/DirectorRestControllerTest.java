package com.sber.java15.spring.springprojectvideorental_3.library.controller.REST.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sber.java15.spring.springprojectvideorental_3.library.dto.AddFilmDTO;
import com.sber.java15.spring.springprojectvideorental_3.library.dto.DirectorDTO;
import com.sber.java15.spring.springprojectvideorental_3.library.model.Position;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
public class DirectorRestControllerTest
      extends CommonTestREST {
    private static Long createdTestDirectorId;
    
    @Test
    @Order(0)
    @Override
    protected void listAll() throws Exception {
        log.info("Тест по просмотру всех режиссеров через REST начат");
        String result = mvc.perform(
                    MockMvcRequestBuilders.get("/rest/directors/getAll")
                          .headers(super.headers)
                          .contentType(MediaType.APPLICATION_JSON)
                          .accept(MediaType.APPLICATION_JSON)
                                   )
              .andDo(print())
              .andExpect(status().is2xxSuccessful())
              .andExpect(jsonPath("$.*", hasSize(greaterThan(0))))
              .andReturn()
              .getResponse()
              .getContentAsString();
        List<DirectorDTO> directorDTOS = objectMapper.readValue(result, new TypeReference<List<DirectorDTO>>() {});
        directorDTOS.forEach(a -> log.info(a.toString()));
        log.info("Тест по просмотру всех режиссеров через REST закончен");
    }
    
    @Test
    @Order(1)
    @Override
    protected void createObject() throws Exception {
        log.info("Тест по созданию режиссера через REST начат");
        //Создаем нового режиссера через REST-контроллер
        DirectorDTO directorDTO = new DirectorDTO("REST_TestDirectorFIO",
                                                 Position.valueOf("Test Director Position"),
                                                 new ArrayList<>());
        
        /*
        Вызываем метод создания (POST) в контроллере, передаем ссылку на REST API в MOCK.
        В headers передаем токен для авторизации (под админом, смотри родительский класс).
        Ожидаем, что статус ответа будет успешным и что в ответе есть поле ID, а далее возвращаем контент как строку
        Все это мы конвертируем в DirectorDTO при помощи ObjectMapper от библиотеки Jackson.
        Присваиваем в статическое поле ID созданного автора, чтобы далее с ним работать.
         */
        DirectorDTO result = objectMapper.readValue(
              mvc.perform(
                          MockMvcRequestBuilders.post("/rest/directors/add")
                                .contentType(MediaType.APPLICATION_JSON)
                                .headers(super.headers)
                                .content(asJsonString(directorDTO))
                                .accept(MediaType.APPLICATION_JSON)
                         )
                    .andExpect(status().is2xxSuccessful())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                    .andReturn()
                    .getResponse()
                    .getContentAsString(),
                DirectorDTO.class);
        createdTestDirectorId = result.getId();
        log.info("Тест по созданию режиссера через REST закончен");
    }
    
    @Test
    @Order(2)
    @Override
    protected void updateObject() throws Exception {
        log.info("Тест по обновлению режиссера через REST начат");
        DirectorDTO existingTestDirector = objectMapper.readValue(
              mvc.perform(
                          MockMvcRequestBuilders.get("/rest/directors/getOneById")
                                .contentType(MediaType.APPLICATION_JSON)
                                .headers(super.headers)
                                .param("id", String.valueOf(createdTestDirectorId))
                                .accept(MediaType.APPLICATION_JSON)
                         )
                    .andExpect(status().is2xxSuccessful())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                    .andReturn()
                    .getResponse()
                    .getContentAsString(),
                DirectorDTO.class);
        //обновляем поля
        existingTestDirector.setDirectorsFio("REST_TestDirectorFIO_UPDATED");
        //TODO ВОЗМОЖНА ОШИБКА
        existingTestDirector.setPosition(Position.valueOf("REST_TestDirectorDescription_UPDATED"));
        
        //вызываем update по REST API
        mvc.perform(
                    MockMvcRequestBuilders.put("/rest/directors/update")
                          .contentType(MediaType.APPLICATION_JSON)
                          .headers(super.headers)
                          .content(asJsonString(existingTestDirector))
                          .param("id", String.valueOf(createdTestDirectorId))
                          .accept(MediaType.APPLICATION_JSON)
                   )
              .andDo(print())
              .andExpect(status().is2xxSuccessful());
        log.info("Тест по обновлению режиссера через REST закончен");
    }
    
    @Test
    @Order(3)
    void addBook() throws Exception {
        log.info("Тест по добавлению фильма режиссеру через REST начат");
        AddFilmDTO addFilmDTO = new AddFilmDTO(1L, createdTestDirectorId);
        String result = mvc.perform(
                    MockMvcRequestBuilders.post("/rest/directors/addFilm")
                          .contentType(MediaType.APPLICATION_JSON)
                          .headers(super.headers)
                          .content(asJsonString(addFilmDTO))
                          .accept(MediaType.APPLICATION_JSON)
                                   )
              .andExpect(status().is2xxSuccessful())
              .andReturn()
              .getResponse()
              .getContentAsString();
        
        DirectorDTO directorDTO = objectMapper.readValue(result, DirectorDTO.class);
        log.info("Тест по добавлению фильма режиссеру через REST закончен. Результат: {}", directorDTO);
    }
    
    @Test
    @Order(4)
    @Override
    protected void deleteObject() throws Exception {
        log.info("Тест по удалению режиссера через REST начат");
        mvc.perform(
                    MockMvcRequestBuilders.delete("/rest/directors/delete/{id}", createdTestDirectorId)
                          .contentType(MediaType.APPLICATION_JSON)
                          .headers(super.headers)
                          .accept(MediaType.APPLICATION_JSON)
                   )
              .andDo(print())
              .andExpect(status().is2xxSuccessful());

        DirectorDTO existingTestDirector = objectMapper.readValue(
              mvc.perform(
                          MockMvcRequestBuilders.get("/rest/directors/getOneById")
                                .contentType(MediaType.APPLICATION_JSON)
                                .headers(super.headers)
                                .param("id", String.valueOf(createdTestDirectorId))
                                .accept(MediaType.APPLICATION_JSON)
                         )
                    .andExpect(status().is2xxSuccessful())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                    .andReturn()
                    .getResponse()
                    .getContentAsString(),
                DirectorDTO.class);
        assertTrue(existingTestDirector.isDeleted());
        log.info("Тест по удалению режиссера через REST закончен");
        mvc.perform(
                    MockMvcRequestBuilders.delete("/rest/directors/delete/hard/{id}", createdTestDirectorId)
                          .contentType(MediaType.APPLICATION_JSON)
                          .headers(super.headers)
                          .accept(MediaType.APPLICATION_JSON)
                   )
              .andDo(print())
              .andExpect(status().is2xxSuccessful());
        log.info("Данные очищены!");
    }
}
