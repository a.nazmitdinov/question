package com.sber.java15.spring.springprojectvideorental_3.library.service;

import com.sber.java15.spring.springprojectvideorental_3.library.dto.DirectorDTO;
import com.sber.java15.spring.springprojectvideorental_3.library.exception.MyDeleteException;
import com.sber.java15.spring.springprojectvideorental_3.library.mapper.DirectorMapper;
import com.sber.java15.spring.springprojectvideorental_3.library.model.Directors;
import com.sber.java15.spring.springprojectvideorental_3.library.repository.DirectorRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;


import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class DirectorServiceTest
      extends GenericTest<Directors, DirectorDTO> {
    public DirectorServiceTest() {
        super();
        //DirectorService directorService = Mockito.mock(BookService.class);
        repository = Mockito.mock(DirectorRepository.class);
        mapper = Mockito.mock(DirectorMapper.class);
        service = new DirectorService((DirectorRepository) repository, (DirectorMapper) mapper);
    }
    
    @Test
    @Order(1)
    @Override
    protected void getAll() {
        Mockito.when(repository.findAll()).thenReturn(DirectorTestData.DIRECTORS_LIST);
        Mockito.when(mapper.toDTOs(DirectorTestData.DIRECTORS_LIST)).thenReturn(DirectorTestData.DIRECTOR_DTO_LIST);
        List<DirectorDTO> directorDTOS = service.listAll();
        log.info("Testing getAll(): " + directorDTOS);
        assertEquals(DirectorTestData.DIRECTORS_LIST.size(), directorDTOS.size());
    }
    
    @Test
    @Order(2)
    @Override
    protected void getOne() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(DirectorTestData.DIRECTOR_1));
        Mockito.when(mapper.toDTO(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_DTO_1);
        DirectorDTO directorDTO = service.getOne(1L);
        log.info("Testing getOne(): " + directorDTO);
        assertEquals(DirectorTestData.DIRECTOR_DTO_1, directorDTO);
    }
    
    @Test
    @Order(3)
    @Override
    protected void create() {
        Mockito.when(mapper.toEntity(DirectorTestData.DIRECTOR_DTO_1)).thenReturn(DirectorTestData.DIRECTOR_1);
        Mockito.when(mapper.toDTO(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_DTO_1);
        Mockito.when(repository.save(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_1);
        DirectorDTO directorDTO = service.create(DirectorTestData.DIRECTOR_DTO_1);
        log.info("Testing create(): " + directorDTO);
        assertEquals(DirectorTestData.DIRECTOR_DTO_1, directorDTO);
    }
    
    @Test
    @Order(4)
    @Override
    protected void update() {
        Mockito.when(mapper.toEntity(DirectorTestData.DIRECTOR_DTO_1)).thenReturn(DirectorTestData.DIRECTOR_1);
        Mockito.when(mapper.toDTO(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_DTO_1);
        Mockito.when(repository.save(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_1);
        DirectorDTO directorDTO = service.update(DirectorTestData.DIRECTOR_DTO_1);
        log.info("Testing update(): " + directorDTO);
        assertEquals(DirectorTestData.DIRECTOR_DTO_1, directorDTO);
    }
    
    @Test
    @Order(5)
    @Override
    protected void delete() throws MyDeleteException {
        //TODO: доделать проверку на удаление режиссера !!!!
//        Mockito.when(((DirectorRepository) repository).checkDirectorForDeletion(1L)).thenReturn(true);
//        Mockito.when(((DirectorRepository) repository).checkDirectorForDeletion(2L)).thenReturn(false);
        Mockito.when(repository.save(DirectorTestData.DIRECTOR_1)).thenReturn(DirectorTestData.DIRECTOR_1);
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(DirectorTestData.DIRECTOR_1));
        log.info("Testing delete() before: " + DirectorTestData.DIRECTOR_1.isDeleted());
        service.deleteSoft(1L);
        log.info("Testing delete() after: " + DirectorTestData.DIRECTOR_1.isDeleted());
        assertTrue(DirectorTestData.DIRECTOR_1.isDeleted());
    }
    
    @Test
    @Order(6)
    @Override
    protected void restore() {
        DirectorTestData.DIRECTOR_3.setDeleted(true);
        Mockito.when(repository.save(DirectorTestData.DIRECTOR_3)).thenReturn(DirectorTestData.DIRECTOR_3);
        Mockito.when(repository.findById(3L)).thenReturn(Optional.of(DirectorTestData.DIRECTOR_3));
        log.info("Testing restore() before: " + DirectorTestData.DIRECTOR_3.isDeleted());
        service.restore(3L);
        log.info("Testing restore() after: " + DirectorTestData.DIRECTOR_3.isDeleted());
        assertFalse(DirectorTestData.DIRECTOR_1.isDeleted());
    }
    
    @Test
    @Order(7)
    @Override
    protected void getAllNotDeleted() {
    
    }
}
